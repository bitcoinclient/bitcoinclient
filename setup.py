#!/usr/bin/python

from setuptools import setup

description=open('README').read()

setup(
    name='bitcoinclient',
    version='0.2',
    description='Python Bitcoin JSON-RPC client',
    author='Jan Dittberner',
    author_email='jan@dittberner.info',
    url='https://gitorious.org/bitcoinclient',
    packages=['bitcoinclient'],
    license='GPLv3+',
    long_description=description,
    use_2to3 = True,
)
