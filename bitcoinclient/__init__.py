# -*- python -*-
# -*- coding: utf-8 -*-
#
# Bitcoin client that works with plain Python 2.6 (jsonrpc not required).
# Copyright © 2011, 2013  Jan Dittberner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 

__version__ = '0.2'

import json
import datetime
from base64 import b64encode
from httplib import HTTPConnection


class BitcoinError(Exception):
    pass


class BitcoinClient():
    """A Bitcoin Client (JSON-RPC) implementation."""
    def __init__(self, host, username, password, port=8332):
        self.uri = 'http://%s:%d/' % (host, port)
        self.default_headers = {
            'Authorization': "Basic %s" % (
                b64encode('%s:%s' % (username, password)),),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'User-Agent': 'Python Bitcoin RPC Client %s' % __version__}
        self.conn = HTTPConnection(host, port)

    def _generate_request_id(self):
        return datetime.datetime.utcnow().isoformat()

    def __call__(self, *args):
        reqid = self._generate_request_id()
        data = json.dumps(
            {'method': args[0],
             'id': reqid,
             'params': args[1:]})
        try:
            self.conn.request('POST', self.uri, data, self.default_headers)
            httpresp = self.conn.getresponse()
        except Exception, e:
            raise BitcoinError(e)
        if httpresp.status != 200:
            raise BitcoinError('http status %d' % httpresp.status)
        resp = httpresp.read()
        try:
            response = json.loads(resp)
            if response['error']:
                raise BitcoinError(response['error'])
            if response['id'] != reqid:
                raise BitcoinError('request and response id do not match')
            return response['result']
        except Exception, e:
            raise BitcoinError('parsing response failed: %s\n%s' % (e, resp))

    def __getattr__(self, name):
        if name in [
            'backupwallet', 'getaccount', 'getaccountaddress',
            'getaddressesbyaccount', 'getbalance', 'getblockcount',
            'getblocknumber', 'getconnectioncount', 'getdifficulty',
            'getgenerate', 'gethashespersec', 'getinfo', 'getnewaddress',
            'getreceivedbyaccount', 'getreceivedbyaddress', 'gettransaction',
            'getwork', 'help', 'listaccounts', 'listreceivedbyaccount',
            'listreceivedbyaddress', 'listtransactions', 'move', 'sendfrom',
            'sendmany', 'sendtoaddress', 'setaccount', 'setgenerate', 'stop',
            'validateaddress']:
            def _bitcoin_func(*params):
                args = [name]
                args.extend(params)
                return self(*args)
            return _bitcoin_func
        raise AttributeError(u'%s does not have %s' % (getclass(self), name))

    def close(self):
        self.conn.close()

